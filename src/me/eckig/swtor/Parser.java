package me.eckig.swtor;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.eckig.swtor.io.IOMonitor;
import me.eckig.swtor.io.ReadFile;
import me.eckig.swtor.io.TailHandler;
import me.eckig.swtor.io.parser.ILineParser;
import me.eckig.swtor.io.parser.SplitLineParser;
import me.eckig.swtor.parser.comm.IErrorHandler;
import me.eckig.swtor.parser.comm.IParserUpdate;
import me.eckig.swtor.parser.comm.ISingleParserUpdate;
import me.eckig.swtor.parser.comm.IStatusListener;


/**
 * <p>
 * Main parser class watching combat log directory and current combat log file.
 * </p>
 * <strong>Basic usage:</strong>
 * <ol>
 * <li>Create a new instance.</li>
 * <li>Add a listener to receive update notifications:<br>
 * {@link Parser#addListener(IParserUpdate)}.</li>
 * <li>Optionally register an error handler:<br>
 * {@link Parser#setErrorHandler(IErrorHandler)}.</li>
 * <li>Start the parser:<br>
 * {@link Parser#start()}.</li>
 * </ol>
 *
 * @author eckig
 */
public class Parser
{

    /**
     * Name of the SW:ToR folder
     */
    public static final String USER_FOLDER_SWTOR = "Star Wars - The Old Republic";

    /**
     * Name of the combat logs folder inside {@link #USER_FOLDER_SWTOR}
     */
    public static final String USER_FOLDER_LOGS = "CombatLogs";

    private static final String COMBAT_LOG_DIR = "Documents" + File.separator + USER_FOLDER_SWTOR + File.separator + USER_FOLDER_LOGS;

    private List<IParserUpdate> mListeners = new ArrayList<>();

    private IErrorHandler mErrorHandler;

    private IOMonitor mIOMonitor;

    private TailHandler mTailListener;

    /**
     * Constructor
     *
     * @param pWorkingDirectory
     *            directory containing the log files
     * @since 29.09.2013
     */
    public Parser(File pWorkingDirectory)
    {
        this(new SplitLineParser(), pWorkingDirectory);
    }

    /**
     * Constructor
     *
     * @param pLineParser
     *            {@link ILineParser} implementation
     * @param pWorkingDirectory
     *            directory containing the log files
     * @throws IllegalArgumentException
     *             thrown if the given directory is invalid
     * @since 29.09.2013
     */
    public Parser(ILineParser pLineParser, File pWorkingDirectory) throws IllegalArgumentException
    {
        mTailListener = new TailHandler(this, pLineParser);
        mIOMonitor = new IOMonitor(mTailListener);
        mIOMonitor.setWorkingDirectory(pWorkingDirectory);
    }

    /**
     * Set the {@link IErrorHandler}
     *
     * @param pHandler
     *            {@link IErrorHandler}
     * @since 12.06.2013
     */
    public void setErrorHandler(IErrorHandler pHandler)
    {
        mErrorHandler = pHandler;
        if (mErrorHandler != null)
        {
            mIOMonitor.setErrorHandler(mErrorHandler);
            mTailListener.setErroHandler(mErrorHandler);
        }
    }

    /**
     * Add {@link IParserUpdate} which should receive updates on new
     * segments/fights
     *
     * @param pListener
     *            {@link IParserUpdate}
     * @since 29.05.2013
     */
    public void addListener(IParserUpdate pListener)
    {
        mListeners.add(pListener);
    }

    /**
     * Remove {@link IParserUpdate} listener
     *
     * @param pListener
     *            {@link IParserUpdate}
     * @since 19.11.2013
     */
    public void removeListener(IParserUpdate pListener)
    {
        mListeners.remove(pListener);
    }

    /**
     * Add {@link IStatusListener} which should receive updates the current
     * parser state
     *
     * @param pListener
     *            {@link IStatusListener}
     * @since 19.11.2013
     */
    public void addStatusListener(IStatusListener pListener)
    {
        mIOMonitor.addStatusListener(pListener);
    }

    /**
     * Remove {@link IStatusListener} listener
     *
     * @param pListener
     *            {@link IStatusListener}
     * @since 19.11.2013
     */
    public void removeStatusListener(IStatusListener pListener)
    {
        mIOMonitor.removeStatusListener(pListener);
    }

    /**
     * @return unmodifiable {@link List} of subscribers
     * @since 22.10.2013
     */
    public List<IParserUpdate> getSubscribers()
    {
        return Collections.unmodifiableList(mListeners);
    }

    /**
     * Read the given {@link File}. This will not tail the File - it will read
     * the file just once from beginning till end.
     *
     * @param pReceiver
     *            {@link ISingleParserUpdate}
     * @param pFile
     *            {@link File} to read
     * @since 04.11.2013
     */
    public void readFile(ISingleParserUpdate pReceiver, File pFile)
    {
        if (pFile == null)
        {
            throw new IllegalArgumentException("File can not be null!");
        }
        if (pReceiver == null)
        {
            throw new IllegalArgumentException("Receiver can not be null!");
        }
        ReadFile rf = new ReadFile(this, pReceiver, new SplitLineParser(), pFile);
        rf.setDay(IOMonitor.getDayFromFileName(pFile.getName()));
        new Thread(rf).start();
    }

    /**
     * Start the parser.
     *
     * @throws Exception
     *             thrown initialization failed
     * @since 30.05.2013
     */
    public void start() throws Exception
    {
        mIOMonitor.start();
    }

    /**
     * Stop tailing & monitoring file system.
     * @since 28.05.2013
     */
    public void stop()
    {
        mIOMonitor.stop();
    }

    /**
     * @return {@link File} representing the combat logs directory in the
     *         current user directory or {@code null}
     * @since 17.06.2013
     */
    public static File getLogDirBySystemProperty()
    {
        String propVal = System.getProperty("user.home");
        if (propVal != null)
        {
            File directory = new File(System.getProperty("user.home"), COMBAT_LOG_DIR);
            if (directory.exists() && directory.isDirectory())
            {
                return directory;
            }
        }
        return null;
    }

    /**
     * @return last modified {@link File} in the current working directory or
     *         {@code null} if no log-file exists in the directory.
     * @since 10.10.2013
     */
    public File lastFileModified()
    {
        return mIOMonitor.lastFileModified();
    }
}
