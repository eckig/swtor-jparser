package me.eckig.swtor.data;

import java.io.Serializable;



/**
 * Amount (of damage, healing)
 *
 * @author eckig
 * @since 30.05.2013
 */
public class Amount implements Comparable<Amount>, Serializable
{

    /**
     * @since 25.10.2013
     */
    private static final long serialVersionUID = 1423210644024293261L;

    private int mAmount;
    private boolean mCritical = false;

    /**
     * Getter for the member field <code>mAmount</code>.
     *
     * @return Returns <code>mAmount</code>.
     */
    public int getAmount()
    {
        return mAmount;
    }

    /**
     * Setter for the member field <code>mAmount</code>.
     *
     * @param pAmount
     *            The new value for <code>mAmount</code> to set.
     */
    public void setAmount(int pAmount)
    {
        mAmount = pAmount;
    }

    /**
     * Getter for the member field <code>mCritical</code>.
     *
     * @return Returns <code>mCritical</code>.
     */
    public boolean isCritical()
    {
        return mCritical;
    }

    /**
     * Setter for the member field <code>mCritical</code>.
     *
     * @param pCritical
     *            The new value for <code>mCritical</code> to set.
     */
    public void setCritical(boolean pCritical)
    {
        mCritical = pCritical;
    }

    @Override
    public int compareTo(Amount pO)
    {
        return Integer.valueOf(getAmount()).compareTo(Integer.valueOf(pO.getAmount()));
    }

    /**
     * Comparison by {@link #getAmount()} and {@link #isCritical()}
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + mAmount;
        result = prime * result + (mCritical ? 1231 : 1237);
        return result;
    }

    /**
     * Comparison by {@link #getAmount()} and {@link #isCritical()}
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Amount other = (Amount) obj;
        if (mAmount != other.mAmount)
        {
            return false;
        }
        if (mCritical != other.mCritical)
        {
            return false;
        }
        return true;
    }

}
