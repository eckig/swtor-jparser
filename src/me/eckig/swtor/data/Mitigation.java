package me.eckig.swtor.data;

import java.io.Serializable;

import me.eckig.swtor.data.enums.MitigationType;


/**
 * Mitigation
 * 
 * @author eckig
 * @since 05.07.2013
 */
public class Mitigation implements Serializable
{

    /**
     * @since 25.10.2013
     */
    private static final long serialVersionUID = 8059757697972393851L;
    private MitigationType mMitigationType;
    private int mAmountMitigated;

    /**
     * Getter for the member field <code>mMitigationType</code>.
     *
     * @return Returns <code>mMitigationType</code>.
     */
    public MitigationType getMitigationType()
    {
        return mMitigationType;
    }

    /**
     * Setter for the member field <code>mMitigationType</code>.
     *
     * @param pMitigationType
     *            The new value for <code>mMitigationType</code> to set.
     */
    public void setMitigationType(MitigationType pMitigationType)
    {
        mMitigationType = pMitigationType;
    }

    /**
     * Getter for the member field <code>mAmountMitigated</code>.
     *
     * @return Returns <code>mAmountMitigated</code>.
     */
    public int getAmountMitigated()
    {
        return mAmountMitigated;
    }

    /**
     * Setter for the member field <code>mAmountMitigated</code>.
     *
     * @param pAmountMitigated
     *            The new value for <code>mAmountMitigated</code> to set.
     */
    public void setAmountMitigated(int pAmountMitigated)
    {
        mAmountMitigated = pAmountMitigated;
    }

    /**
     * Comparison by {@link #getAmountMitigated()} and
     * {@link #getMitigationType()}
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + mAmountMitigated;
        result = prime * result + (mMitigationType == null ? 0 : mMitigationType.hashCode());
        return result;
    }

    /**
     * Comparison by {@link #getAmountMitigated()} and
     * {@link #getMitigationType()}
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Mitigation other = (Mitigation) obj;
        if (mAmountMitigated != other.mAmountMitigated)
        {
            return false;
        }
        if (mMitigationType != other.mMitigationType)
        {
            return false;
        }
        return true;
    }

}
