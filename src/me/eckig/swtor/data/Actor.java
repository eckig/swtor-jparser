package me.eckig.swtor.data;

import java.io.Serializable;



/**
 * Every {@link LogEntry} has two Actors: Source and Target
 *
 * @author eckig
 * @since 29.05.2013
 */
public class Actor implements Serializable
{

    /**
     * @since 25.10.2013
     */
    private static final long serialVersionUID = -4480630569263661898L;

    /**
     * Used when a LogEntry has no Source or Target
     *
     * @since 29.05.2013
     */
    public static final Actor UNKNOWN = new Actor("?", false, false);

	private String mName;
    private boolean mIsPlayer;
    private boolean mIsCompanion;

    /**
     * Constructor
     *
     * @param pName
     *            actor name
     * @param pIsPlayer
     *            {@code true} if is player
     * @param pIsCompanion
     *            {@code true} if is companion
     * @since 21.10.2013
     */
    public Actor(String pName, boolean pIsPlayer, boolean pIsCompanion)
    {
        mName = pName;
        mIsCompanion = pIsCompanion;
        mIsPlayer = pIsPlayer;
    }

    /**
     * @return Name of the Actor
     * @since 29.05.2013
     */
	public String getName()
	{
		return mName;
	}

    /**
     * @return {@code true} if this Actor is a player
     * @since 29.05.2013
     */
	public boolean isPlayer()
	{
		return mIsPlayer;
	}

    /**
     * @return {@code true} if this Actor is a players companion
     * @since 29.05.2013
     */
	public boolean isCompanion()
	{
		return mIsCompanion;
	}

    @Override
    public String toString()
    {
        return getName();
    }

    /**
     * Comparison by {@link #isCompanion()}, {@link #isPlayer()} and
     * {@link #getName()}
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (mIsCompanion ? 1231 : 1237);
        result = prime * result + (mIsPlayer ? 1231 : 1237);
        result = prime * result + (mName == null ? 0 : mName.hashCode());
        return result;
    }

    /**
     * Comparison by {@link #isCompanion()}, {@link #isPlayer()} and
     * {@link #getName()}
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Actor other = (Actor) obj;
        if (mIsCompanion != other.mIsCompanion)
        {
            return false;
        }
        if (mIsPlayer != other.mIsPlayer)
        {
            return false;
        }
        if (mName == null)
        {
            if (other.mName != null)
            {
                return false;
            }
        }
        else if (!mName.equals(other.mName))
        {
            return false;
        }
        return true;
    }
}
