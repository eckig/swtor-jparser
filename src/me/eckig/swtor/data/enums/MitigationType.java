package me.eckig.swtor.data.enums;

/**
 * Type of damage mitigation
 * 
 * @author eckig
 * @since 30.05.2013
 */
public enum MitigationType
{
    /**
     * MISS
     */
	MISS(836045448945502L),
    /**
     * SHIELD
     */
    SHIELD(836045448945509L),
    /**
     * DODGE
     */
	DODGE(836045448945505L),
    /**
     * DEFLECT
     */
	DEFLECT(836045448945508L),
    /**
     * PARRY
     */
	PARRY(836045448945503L),
    /**
     * IMMUNE
     */
	IMMUNE(836045448945506L),
    /**
     * RESIST
     */
	RESIST(836045448945507L),
    /**
     * ABSORBED
     */
    ABSORBED(836045448945511l),
    /**
     * NONE
     */
    NONE(0L);

	private long id;

    private MitigationType(long pId)
	{
        id = pId;
	}

    /**
     * @return ID
     * @since 30.05.2013
     */
	public long getId()
	{
		return id;
	}

    /**
     * @param pId
     * @return {@link MitigationType}
     * @since 30.05.2013
     */
    public static MitigationType valueOfId(long pId)
	{
		for (MitigationType type : MitigationType.values())
		{
            if (type.id == pId)
			{
				return type;
			}
		}
        return NONE;
	}
}
