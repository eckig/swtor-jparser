package me.eckig.swtor.data.enums;

/**
 * Event Type
 *
 * @author eckig
 * @since 29.05.2013
 */
public enum EventType
{
    /**
     * Remove effect
     */
	REMOVE_EFFECT(836045448945478L),
    /**
     * Apply effect
     */
	APPLY_EFFECT(836045448945477L),
    /**
     * Spend (energy..)
     */
    SPEND(836045448945473L),
    /**
     * Restore (energy..)
     */
	RESTORE(836045448945476L),
    /**
     * Event
     */
	EVENT(836045448945472L),
    /**
     * Everything else
     */
	UNKNOWN(0L);

	private long id;

    private EventType(long pId)
	{
        id = pId;
	}

    /**
     * Find {@link EventType} by its ID
     *
     * @param pId
     *            ID
     * @return {@link EventType}
     * @since 29.05.2013
     */
    public static EventType valueOfId(long pId)
	{
        for (EventType type : EventType.values())
		{
            if (type.id == pId)
			{
				return type;
			}
		}
        // else
        return UNKNOWN;
	}
}
