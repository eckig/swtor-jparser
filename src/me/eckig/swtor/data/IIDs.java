package me.eckig.swtor.data;


/**
 * Interface collecting IDs
 *
 * @author eckig
 * @since 03.11.2013
 */
public interface IIDs
{

    /**
     * {@link Ability} ID Enter Combat
     */
    public static final long ID_ENTER_COMBAT = 836045448945489L;
    /**
     * {@link Ability} ID Exit Combat
     */
    public static final long ID_EXIT_COMBAT = 836045448945490L;
    /**
     * {@link Ability} ID Death
     */
    public static final long ID_DEATH = 836045448945493L;
    /**
     * {@link Ability} ID Damage
     */
    public static final long ID_DAMAGE = 836045448945501L;
    /**
     * {@link Ability} ID Heal
     */
    public static final long ID_HEAL = 836045448945500L;
    /**
     * {@link Ability} ID Revived
     */
    public static final long ID_REVIVED = 836045448945494L;

    /**
     * {@link Ability} ID SafeLoginImmunity
     */
    public static final long SAFE_LOGIN_IMMUNITY_ID = 973870949466112l;
}
