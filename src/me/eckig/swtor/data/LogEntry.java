package me.eckig.swtor.data;

import java.io.Serializable;

import me.eckig.swtor.data.enums.EventType;


/**
 * <p>
 * A log file consists {@link LogEntry} objects.
 * </p>
 * Example log entry:<br>
 * <code>[19:28:53.519] [@Merav] [Dread Host Soldier {3266932513964032}:1507007638359] [Orbital Strike {2145301804613632}] [ApplyEffect {836045448945477}: Damage {836045448945501}] (6659* elemental {836045448940875}) &lt;6659&gt;</code>
 * <br>
 * Which is stored into an entry as follows:
 * <table>
 * <tr>
 * <th>Log entry</th>
 * <th>Attribute</th>
 * </tr>
 * <tr>
 * <td>{@code [19:28:53.519]}</td>
 * <td>{@link LogEntry#getDate()}</td>
 * </tr>
 * <tr>
 * <td>{@code [@Merav]}</td>
 * <td>{@link LogEntry#getCharacterSource()}</td>
 * </tr>
 * <tr>
 * <td><code>[Dread Host Soldier {3266932513964032}:1507007638359]</code></td>
 * <td>{@link LogEntry#getCharacterTarget()}</td>
 * </tr>
 * <tr>
 * <td><code>[Orbital Strike {2145301804613632}]</code></td>
 * <td>{@link LogEntry#getAbility()}</td>
 * </tr>
 * <tr>
 * <td><code>[ApplyEffect {836045448945477}: Damage {836045448945501}]</code></td>
 * <td>{@link LogEntry#getEventType()} : {@link LogEntry#getSecondaryAbility()}</td>
 * </tr>
 * <tr>
 * <td><code>(6659* elemental {836045448940875})</code></td>
 * <td>{@link LogEntry#getAmount()}</td>
 * </tr>
 * <tr>
 * <td><code>&lt;6659&gt;</code></td>
 * <td>{@link LogEntry#getThreat()}</td>
 * </tr>
 *
 * </table>
 *
 * @author eckig
 * @since 29.05.2013
 */
public class LogEntry implements Serializable, IIDs
{

    /**
     * @since 25.10.2013
     */
    private static final long serialVersionUID = 4929493055645326949L;

    private long mDate;
	private Actor mCharacterTarget;
	private Actor mCharacterSource;
	private Ability mAbility;
    private Ability mSecondaryAbility;
    private Amount mAmount;
    private Mitigation mMitigation;
    private EventType mEventType;
    private int mThreat;

    /**
     * @return timestamp of this LogEntry
     * @since 29.05.2013
     */
    public long getDate()
	{
		return mDate;
	}

    /**
     * @return {@link Mitigation}
     * @since 05.07.2013
     */
    public Mitigation getMitigation()
    {
        return mMitigation;
    }

    /**
     * @return {@link Actor} target
     * @since 29.05.2013
     */
	public Actor getCharacterTarget()
	{
		return mCharacterTarget;
	}

    /**
     * @return Actor source
     * @since 29.05.2013
     */
	public Actor getCharacterSource()
	{
		return mCharacterSource;
	}

    /**
     * @return {@link Ability}
     * @since 29.05.2013
     */
	public Ability getAbility()
	{
		return mAbility;
	}

    /**
     * @return secondary {@link Ability}
     * @since 03.11.2013
     */
    public Ability getSecondaryAbility()
    {
        return mSecondaryAbility;
    }

    /**
     * @return {@link Amount}
     * @since 30.05.2013
     */
    public Amount getAmount()
    {
        return mAmount;
    }

    /**
     * @return amount of threat for this action
     * @since 29.05.2013
     */
    public int getThreat()
    {
        return mThreat;
    }

    /**
     * @return {@link EventType}
     * @since 29.05.2013
     */
    public EventType getEventType()
    {
        return mEventType;
    }

    /**
     * Setter for the member field <code>mDate</code>.
     *
     * @param pTimeStamp
     *            The new value for <code>mDate</code> to set.
     */
    public void setDate(long pTimeStamp)
    {
        mDate = pTimeStamp;
    }

    /**
     * Setter for the member field <code>mCharacterTarget</code>.
     *
     * @param pCharacterTarget
     *            The new value for <code>mCharacterTarget</code> to set.
     */
    public void setCharacterTarget(Actor pCharacterTarget)
    {
        mCharacterTarget = pCharacterTarget;
    }

    /**
     * Setter for the member field <code>mCharacterSource</code>.
     *
     * @param pCharacterSource
     *            The new value for <code>mCharacterSource</code> to set.
     */
    public void setCharacterSource(Actor pCharacterSource)
    {
        mCharacterSource = pCharacterSource;
    }

    /**
     * Setter for the member field <code>mAbility</code>.
     *
     * @param pAbility
     *            The new value for <code>mAbility</code> to set.
     */
    public void setAbility(Ability pAbility)
    {
        mAbility = pAbility;
    }

    /**
     * Setter for member field {@code mSecondaryAbility}.
     *
     * @param pAbility
     *            {@link Ability}
     * @since 03.11.2013
     */
    public void setSecondaryAbility(Ability pAbility)
    {
        mSecondaryAbility = pAbility;
    }

    /**
     * Setter for the member field <code>mAmount</code>.
     *
     * @param pAmount
     *            The new value for <code>mAmount</code> to set.
     */
    public void setAmount(Amount pAmount)
    {
        mAmount = pAmount;
    }

    /**
     * Setter for the member field <code>mMitigation</code>.
     *
     * @param pMitigation
     *            The new value for <code>mMitigation</code> to set.
     */
    public void setMitigation(Mitigation pMitigation)
    {
        mMitigation = pMitigation;
    }

    /**
     * Setter for the member field <code>mEventType</code>.
     *
     * @param pEventType
     *            The new value for <code>mEventType</code> to set.
     */
    public void setEventType(EventType pEventType)
    {
        mEventType = pEventType;
    }

    /**
     * Setter for the member field <code>mThreat</code>.
     *
     * @param pThreat
     *            The new value for <code>mThreat</code> to set.
     */
    public void setThreat(int pThreat)
    {
        mThreat = pThreat;
    }

    /**
     * @return {@code true} if this {@link LogEntry} contains actions of a
     *         companion character
     * @since 22.10.2013
     */
    public boolean isCompanionLogEntry()
    {
        return getCharacterSource() != null && getCharacterSource().isCompanion() || getCharacterTarget() != null
                && getCharacterTarget().isCompanion();
    }

    /**
     * @return {@code true} if the secondary ability is
     *         {@link IIDs#ID_ENTER_COMBAT}
     * @since 10.11.2013
     */
    public boolean isFirstLogEntryOfCombat()
    {
        return getSecondaryAbility() != null && getSecondaryAbility().getId() == ID_ENTER_COMBAT;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (mAbility == null ? 0 : mAbility.hashCode());
        result = prime * result + (mAmount == null ? 0 : mAmount.hashCode());
        result = prime * result + (mCharacterSource == null ? 0 : mCharacterSource.hashCode());
        result = prime * result + (mCharacterTarget == null ? 0 : mCharacterTarget.hashCode());
        result = prime * result + (int) (mDate ^ mDate >>> 32);
        result = prime * result + (mSecondaryAbility == null ? 0 : mSecondaryAbility.hashCode());
        result = prime * result + (mEventType == null ? 0 : mEventType.hashCode());
        result = prime * result + (mMitigation == null ? 0 : mMitigation.hashCode());
        result = prime * result + mThreat;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        LogEntry other = (LogEntry) obj;
        if (mAbility == null)
        {
            if (other.mAbility != null)
            {
                return false;
            }
        }
        else if (!mAbility.equals(other.mAbility))
        {
            return false;
        }
        if (mSecondaryAbility == null)
        {
            if (other.mSecondaryAbility != null)
            {
                return false;
            }
        }
        else if (!mSecondaryAbility.equals(other.mSecondaryAbility))
        {
            return false;
        }
        if (mAmount == null)
        {
            if (other.mAmount != null)
            {
                return false;
            }
        }
        else if (!mAmount.equals(other.mAmount))
        {
            return false;
        }
        if (mCharacterSource == null)
        {
            if (other.mCharacterSource != null)
            {
                return false;
            }
        }
        else if (!mCharacterSource.equals(other.mCharacterSource))
        {
            return false;
        }
        if (mCharacterTarget == null)
        {
            if (other.mCharacterTarget != null)
            {
                return false;
            }
        }
        else if (!mCharacterTarget.equals(other.mCharacterTarget))
        {
            return false;
        }
        if (mDate != other.mDate)
        {
            return false;
        }
        if (mEventType != other.mEventType)
        {
            return false;
        }
        if (mMitigation == null)
        {
            if (other.mMitigation != null)
            {
                return false;
            }
        }
        else if (!mMitigation.equals(other.mMitigation))
        {
            return false;
        }
        if (mThreat != other.mThreat)
        {
            return false;
        }
        return true;
    }

}
