package me.eckig.swtor.data;

import java.io.Serializable;


/**
 * Ability of a {@link LogEntry}.
 *
 * @author eckig
 * @since 29.05.2013
 */
public class Ability implements Serializable
{

    /**
     * @since 07.10.2013
     */
    private static final long serialVersionUID = -1361031663498117666L;

	private String mName;
    private long mId;

    /**
     * Empty constructor for serialization
     *
     * @since 07.10.2013
     */
    public Ability()
    {
        // empty
    }

    /**
     * Constructor
     *
     * @param pName
     *            ability name
     * @param pId
     *            ability id
     * @since 16.10.2013
     */
    public Ability(String pName, long pId)
    {
        mName = pName;
        mId = pId;
    }

    /**
     * @return Name of the Ability
     * @since 29.05.2013
     */
	public String getName()
	{
        return mName;
	}

    /**
     * @return ID of the ability
     * @since 21.07.2013
     */
    public long getId()
    {
        return mId;
    }

    @Override
    public String toString()
    {
        return getName();
    }

    /**
     * Comparison by {@link #getId()}
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (mId ^ mId >>> 32);
        return result;
    }

    /**
     * Comparison by {@link #getId()}
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Ability other = (Ability) obj;
        if (mId != other.mId)
        {
            return false;
        }
        return true;
    }

}
