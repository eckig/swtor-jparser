package me.eckig.swtor.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

/**
 * Thread to monitor the current working directoy for new files
 *
 * @author eckig
 * @since 17.11.2013
 */
public class DirectoryWatchService implements Runnable
{

    private final IOMonitor mMonitor;
    private Path imDirectory;
    private WatchService imWatchService;

    /**
     * Constructor
     *
     * @param pDirectory
     *            working directory
     * @param pMonitor
     *            {@link IOMonitor}
     * @throws IllegalArgumentException
     *             thrown on invalid directory
     * @throws IOException
     *             thrown if watchservice could not be created
     * @since 17.11.2013
     */
    public DirectoryWatchService(IOMonitor pMonitor, File pDirectory) throws IllegalArgumentException, IOException
    {
        mMonitor = pMonitor;
        if (pDirectory == null || !pDirectory.exists() || !pDirectory.isDirectory())
        {
            throw new IllegalArgumentException("Given directory is invalid");
        }
        imDirectory = pDirectory.toPath();
        imWatchService = imDirectory.getFileSystem().newWatchService();
        imDirectory.register(imWatchService, StandardWatchEventKinds.ENTRY_CREATE);
    }

    @Override
    public void run()
    {
        while (true && !Thread.currentThread().isInterrupted())
        {
            WatchKey watch;
            try
            {
                watch = imWatchService.take();
            }
            catch (InterruptedException ex)
            {
                return;
            }
            List<WatchEvent<?>> events = watch.pollEvents();
            for (WatchEvent<?> event : events)
            {
                Kind<?> kind = event.kind();
                if (kind == StandardWatchEventKinds.OVERFLOW)
                {
                    continue;
                }
                else if (kind == StandardWatchEventKinds.ENTRY_CREATE)
                {
                    Path filename = (Path) event.context();
                    mMonitor.parse(imDirectory.resolve(filename).toFile());
                }
            }
            if (!watch.reset())
            {
                return;
            }
        }
    }
}