package me.eckig.swtor.io;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import me.eckig.swtor.parser.comm.IErrorHandler;
import me.eckig.swtor.parser.comm.IStatusListener;
import me.eckig.swtor.parser.comm.ITailErrorListener;


/**
 *
 * @author eckig
 * @since 19.11.2013
 */
public class IOMonitor implements ITailErrorListener
{

    /**
     * RegEx for files with the format
     * {@code combat_2013-05-17_17_18_30_845473.txt}
     */
    public static final Pattern FILE_NAME_PATTERN = Pattern
            .compile("combat_[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{6}\\.txt");

    private File mCurrentDirectory;

    private TailHandler mTailListener;
    private Tailer mTailer;

    private Thread mCurrentWatchServiceThread;

    private IErrorHandler mErrorHandler;

    private List<IStatusListener> mStatusListeners = new ArrayList<>();

    /**
     * Constructor
     *
     * @param pHandler
     *            {@link TailHandler}
     * @throws IllegalArgumentException
     *             thrown if given {@link TailHandler} is {@code null}
     * @since 19.11.2013
     */
    public IOMonitor(TailHandler pHandler) throws IllegalArgumentException
    {
        if (pHandler == null)
        {
            throw new IllegalArgumentException("No TailHandler given!");
        }
        mTailListener = pHandler;
    }

    /**
     * Add {@link IStatusListener} which should receive updates the current
     * parser state
     *
     * @param pListener
     *            {@link IStatusListener}
     * @since 19.11.2013
     */
    public void addStatusListener(IStatusListener pListener)
    {
        mStatusListeners.add(pListener);
    }

    /**
     * Remove {@link IStatusListener} listener
     *
     * @param pListener
     *            {@link IStatusListener}
     * @since 19.11.2013
     */
    public void removeStatusListener(IStatusListener pListener)
    {
        mStatusListeners.remove(pListener);
    }

    /**
     * Set the working directory
     *
     * @param pDir
     * @since 19.11.2013
     */
    public void setWorkingDirectory(File pDir)
    {
        if (pDir == null || !pDir.isDirectory() || !pDir.canRead())
        {
            throw new IllegalArgumentException("Illegal directory!");
        }
        stop();
        mCurrentDirectory = pDir;
    }

    /**
     * Set the error handler
     *
     * @param pErrorHandler
     * @since 19.11.2013
     */
    public void setErrorHandler(IErrorHandler pErrorHandler)
    {
        mErrorHandler = pErrorHandler;
    }

    /**
     * Start monitor
     *
     * @throws IllegalArgumentException
     * @throws IOException
     * @since 19.11.2013
     */
    public void start() throws IllegalArgumentException, IOException
    {
        stop();
        parse(lastFileModified());

        mCurrentWatchServiceThread = new Thread(new DirectoryWatchService(this, getCurrentWorkingDirectory()), "WatchService");
        mCurrentWatchServiceThread.start();

        for (IStatusListener l : mStatusListeners)
        {
            l.started();
        }
    }

    /**
     * Stop monitor
     *
     * @since 19.11.2013
     */
    public void stop()
    {
        if (mCurrentWatchServiceThread != null)
        {
            mCurrentWatchServiceThread.interrupt();
            mCurrentWatchServiceThread = null;
        }
        if (mTailer != null)
        {
            mTailer.stop();
            mTailer = null;
        }

        for (IStatusListener l : mStatusListeners)
        {
            l.stopped();
        }
    }

    /**
     * set the file to be parsed.<br>
     * Make sure, that the file matches the necessary format as described in
     * {@link #FILE_NAME_PATTERN}.
     *
     * @param pFile
     *            file
     * @since 10.10.2013
     */
    protected void parse(File pFile)
    {
        // no need to start over when the file is the same, just keep going
        if (mTailer != null && mTailer.getFile().equals(pFile))
        {
            return;
        }
        if (mTailer != null)
        {
            mTailer.stop();
            mTailer = null;
        }

        if (pFile == null || !FILE_NAME_PATTERN.matcher(pFile.getName()).matches())
        {
            if (mErrorHandler != null)
            {
                mErrorHandler.handleNoFileFound(getCurrentWorkingDirectory());
            }
            return;
        }

        mTailListener.setDay(getDayFromFileName(pFile.getName()));
        mTailer = new Tailer(pFile, mTailListener, this);
        new Thread(mTailer).start();

        for (IStatusListener l : mStatusListeners)
        {
            l.parsingFile(pFile.getName());
        }
    }

    /**
     * @return current combat logs folder
     * @since 10.10.2013
     */
    protected File getCurrentWorkingDirectory()
    {
        return mCurrentDirectory;
    }

    /**
     * Handle exception from Tailer, which has now stopped.
     */
    @Override
    public void handle(Exception pExc, File pFile)
    {
        for (IStatusListener l : mStatusListeners)
        {
            l.lostFile(pFile.getName());
        }

        // check if another tailer has already started working on a new file:
        if (!mTailer.getFile().equals(pFile))
        {
            // then everything is working fine
            return;
        }
        //so we are currently not tailing:
        mTailer = null;

        // wait 10secs for another tailer to start (by filesystem monitor)
        Runnable checkRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    Thread.sleep(10000);
                }
                catch (InterruptedException e)
                {
                    return;
                }
                checkForTailer();
            }
        };
        Thread checkThread = new Thread(checkRunnable);
        checkThread.start();
    }

    /**
     * called exclusively by {@link #handle(Exception, File)} to check if a new
     * tailer has started working.
     *
     * @since 21.11.2013
     */
    void checkForTailer()
    {
        // if there is still no new file being tailed, use the last file:
        if (mTailer == null)
        {
            parse(lastFileModified());
        }
    }

    /**
     * If the filename is {@code combat_2013-05-23_17_56_39_568778.txt} the
     * returned value is {@code 2013-05-23}.
     *
     * @param pFileName
     * @return the day derived of the given filename
     * @throws IndexOutOfBoundsException
     * @since 04.11.2013
     */
    public static String getDayFromFileName(String pFileName) throws IndexOutOfBoundsException
    {
        return pFileName.substring(7, 17);
    }

    /**
     * @return last modified {@link File} in the current working directory or
     *         {@code null} if no log-file exists.
     * @since 10.10.2013
     */
    public File lastFileModified()
    {
        if (!getCurrentWorkingDirectory().exists())
        {
            return null;
        }
        File[] files = getCurrentWorkingDirectory().listFiles(new CombatFileNameFilter());
        long lastMod = 0l;
        File choise = null;
        for (File file : files)
        {
            if (file.lastModified() > lastMod)
            {
                choise = file;
                lastMod = file.lastModified();
            }
        }
        return choise;
    }

    /**
     * {@link FileFilter} accepting only files matching the regex of a typical
     * SWToR log file
     *
     * @author eckig
     * @since 28.05.2013
     */
    protected static class CombatFileNameFilter implements FileFilter
    {

        @Override
        public boolean accept(File pPathname)
        {
            return pPathname != null && pPathname.isFile() && FILE_NAME_PATTERN.matcher(pPathname.getName()).matches();
        }
    }
}
