package me.eckig.swtor.io;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.Set;

import me.eckig.swtor.parser.comm.ITailErrorListener;
import me.eckig.swtor.parser.comm.ITailListener;


/**
 * Thread for continuously reading a file.
 *
 * @author eckig
 * @since 18.11.2013
 */
public class Tailer implements Runnable
{

    private static final Set<? extends OpenOption> OPEN_ATTRS = Collections.singleton(StandardOpenOption.READ);
    private static final int DEFAULT_BUFSIZE = 4096;
    private static final long WAIT_TIME = 200;
    private static final Charset CHARSET = Charset.forName("ISO-8859-15");

    private ITailListener mListener;
    private ITailErrorListener mErrorListener;
    private File mFile;
    private ByteBuffer mBuffer = ByteBuffer.allocate(DEFAULT_BUFSIZE);

    private byte[] mLineBuffer = new byte[200];
    private int mCurentLineBufferSize = 0;

    private boolean mRun = true;

    /**
     * @param pFile
     *            file to tail
     * @param pListener
     *            {@link ITailListener}
     * @param pErrorListener
     *            {@link ITailErrorListener}
     * @throws IllegalArgumentException
     * @since 18.11.2013
     */
    public Tailer(File pFile, ITailListener pListener, ITailErrorListener pErrorListener) throws IllegalArgumentException
    {
        if (pFile == null || !pFile.isFile() || !pFile.canRead())
        {
            throw new IllegalArgumentException("Illegal file given!");
        }
        if (pListener == null)
        {
            throw new IllegalArgumentException("No listener given!");
        }
        if (pErrorListener == null)
        {
            throw new IllegalArgumentException("No error listener given!");
        }
        mFile = pFile;
        mListener = pListener;
        mErrorListener = pErrorListener;
    }

    private long readLines(SeekableByteChannel pFileChanel, long pCurrentPosition) throws IOException
    {
        boolean seenCR = false;
        long pos = pCurrentPosition;
        long rePos = pos; // position to re-read
        // read file in chunks until EOF:
        while (mRun && !Thread.currentThread().isInterrupted() && pFileChanel.read(mBuffer) > 0)
        {
            mBuffer.flip();
            // process a single chunk:
            for (int i = 0; i < mBuffer.limit(); i++)
            {
                final byte ch = mBuffer.get();
                switch (ch)
                {
                    case '\n':
                        seenCR = false; // swallow CR before LF
                        mListener.handle(lineBufferToString());
                        resetLineBuffer();
                        rePos = pos + i + 1;
                        break;
                    case '\r':
                        if (seenCR)
                        {
                            writeLineBuffer('\r');
                        }
                        seenCR = true;
                        break;
                    default:
                        if (seenCR)
                        {
                            seenCR = false; // swallow final CR
                            mListener.handle(lineBufferToString());
                            resetLineBuffer();
                            rePos = pos + i + 1;
                        }
                        writeLineBuffer(ch);
                }
            }
            pos = pFileChanel.position();
            mBuffer.clear();
        }

        // check the rest of the file until EOF:
        if (mCurentLineBufferSize > 0)
        {
            String line = lineBufferToString();
            if (line.length() > 80)
            {
                char lastChar = line.charAt(line.length() - 1);
                if (lastChar == ')' || lastChar == '>')
                {
                    // At this point we are pretty sure, that this is a complete log line at EOF
                    mListener.handle(line);
                    rePos = pos;
                }
            }

        }
        resetLineBuffer();
        pFileChanel.position(rePos);
        return rePos;
    }

    @Override
    public void run()
    {
        long position = mFile.length();
        long length;
        try
        {
            while (mRun && !Thread.currentThread().isInterrupted())
            {
                length = Files.size(mFile.toPath()); // throws IOException if file does not exist
                if (length > 0)
                {
                    if (length < position)
                    {
                        // file was rotated
                        position = length;
                        continue;
                    }
                    else if (length > position)
                    {
                        // read new content
                        try (SeekableByteChannel reader = Files.newByteChannel(mFile.toPath(), OPEN_ATTRS))
                        {
                            reader.position(position);
                            position = readLines(reader, position);
                        }
                    }
                }
                Thread.sleep(WAIT_TIME);
            }
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            stop(e);
        }
        catch (final Exception e)
        {
            stop(e);
        }
    }

    private void verifyBufferSize(int pSize)
    {
        if (pSize > mLineBuffer.length)
        {
            byte[] old = mLineBuffer;
            mLineBuffer = new byte[Math.max(pSize, 2 * mLineBuffer.length)];
            System.arraycopy(old, 0, mLineBuffer, 0, old.length);
            old = null;
        }
    }

    private void writeLineBuffer(int b)
    {
        verifyBufferSize(mCurentLineBufferSize + 1);
        mLineBuffer[mCurentLineBufferSize++] = (byte) b;
    }

    private void resetLineBuffer()
    {
        mCurentLineBufferSize = 0;
    }

    private String lineBufferToString()
    {
        return new String(mLineBuffer, 0, mCurentLineBufferSize, CHARSET);
    }

    private void stop(final Exception e)
    {
        mErrorListener.handle(e, getFile());
        stop();
    }

    /**
     * Stop the tailer
     */
    public void stop()
    {
        mRun = false;
    }

    /**
     * @return file
     * @since 18.11.2013
     */
    public File getFile()
    {
        return mFile;
    }

}
