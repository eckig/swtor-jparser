package me.eckig.swtor.io.parser;

import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * Fast date parser specifically designed for the timestamps in SW:ToR log
 * files.
 *
 * @see "http://java-performance.info/java-util-date-java-util-calendar-and-java-text-simpledateformat/"
 * @since 17.11.2013
 */
public class FastDateParser
{
    private static final int TWO_SUBTRACT = '0' * 11;
    private static final int THREE_SUBTRACT = '0' * 111;

    private int mDay;
    private int mMonth;
    private int mYear;

    /**
     * Constructor
     *
     * @param pCurrentDate
     * @throws IllegalArgumentException
     *             if date is not in the format {@code yyyy-MM-dd}
     * @since 17.11.2013
     */
    public FastDateParser(String pCurrentDate) throws IllegalArgumentException
    {
        checkDate(pCurrentDate);
        mYear = four(pCurrentDate);
        mMonth = two(pCurrentDate, 5) - 1;
        mDay = two(pCurrentDate, 8);
    }

    private static int two(final String s, final int from)
    {
        return s.charAt(from) * 10 + s.charAt(from + 1) - TWO_SUBTRACT;
    }

    private static int three(final String s, final int from)
    {
        return s.charAt(from) * 100 + s.charAt(from + 1) * 10 + s.charAt(from + 2) - THREE_SUBTRACT;
    }

    private static int four(final String s)
    {
        return 100 * two(s, 0) + two(s, 2);
    }

    private static void checkTimeParts(int hour, int min, int sec, int millis)
    {
        if (hour < 0 || hour > 23)
        {
            throw new IllegalArgumentException("Hours must be between 0 and 23!");
        }
        if (min < 0 || min > 59)
        {
            throw new IllegalArgumentException("Minutes must be between 0 and 59!");
        }
        if (sec < 0 || sec > 59)
        {
            throw new IllegalArgumentException("Seconds must be between 0 and 59!");
        }
        if (millis < 0 || millis > 999)
        {
            throw new IllegalArgumentException("Milliseconds must be between 0 and 999!");
        }
    }

    private void checkDate(final String date) throws IllegalArgumentException
    {
        if (date == null || date.length() != 10 || date.charAt(4) != '-' || date.charAt(7) != '-')
        {
            throw new IllegalArgumentException("Date should be in yyyy-MM-dd format!");
        }
    }

    private void checkTime(String time)
    {
        if (time == null || time.length() != 12 || time.charAt(2) != ':' || time.charAt(5) != ':' || time.charAt(8) != '.')
        {
            throw new IllegalArgumentException("Time should be in HH:mm:ss.SSS format!");
        }
    }

    /**
     * @param pTime
     *            timestamp string {@code HH:mm:ss.SSS}
     * @return timestamp with date and time
     * @throws IllegalArgumentException
     *             if the time is not in the format {@code HH:mm:ss.SSS}
     * @since 17.11.2013
     */
    public long parse(String pTime) throws IllegalArgumentException
    {
        checkTime(pTime);
        final int hour = two(pTime, 0);
        final int min = two(pTime, 3);
        final int sec = two(pTime, 6);
        final int millis = three(pTime, 9);
        checkTimeParts(hour, min, sec, millis);
        Calendar cl = new GregorianCalendar(mYear, mMonth, mDay, hour, min, sec);
        return cl.getTimeInMillis() + millis;
    }
}
