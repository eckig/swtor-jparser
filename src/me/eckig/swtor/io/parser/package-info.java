/**
 * A package containing the components for parsing a single log line
 * @author eckig
 * @since 19.11.2013
 */
package me.eckig.swtor.io.parser;