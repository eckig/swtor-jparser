package me.eckig.swtor.io.parser;

import java.text.ParseException;
import java.util.regex.Pattern;

import me.eckig.swtor.data.Ability;
import me.eckig.swtor.data.Actor;
import me.eckig.swtor.data.Amount;
import me.eckig.swtor.data.LogEntry;
import me.eckig.swtor.data.Mitigation;
import me.eckig.swtor.data.enums.EventType;
import me.eckig.swtor.data.enums.MitigationType;


/**
 * {@link ILineParser} implementation parsing log lines with
 * {@link Pattern#split(CharSequence)}
 *
 * @author eckig
 * @since 09.07.2013
 */
public class SplitLineParser implements ILineParser
{
    private static final Pattern SPLIT_PATTERN = Pattern.compile(Pattern.quote("] ["));

    /**
     * parses the given string token to an {@link Actor} object
     *
     * @param pToken
     * @return {@link Actor}
     * @since 25.10.2013
     */
    protected Actor parseActor(String pToken)
    {
        if (pToken == null || pToken.length() < 1)
        {
            return Actor.UNKNOWN;
        }
        String cName;
        boolean player = false;
        boolean companion = false;
        if (pToken.charAt(0) == '@')
        {
            int sepIndex = pToken.indexOf(':');
            if (sepIndex > -1)
            {
                cName = pToken.substring(sepIndex + 1, pToken.indexOf('{') - 1);
                companion = true;
            }
            else
            {
                cName = pToken.substring(1, pToken.length());
                player = true;
            }
        }
        else
        {
            cName = pToken.substring(0, pToken.indexOf('{') - 1);
        }

        return new Actor(cName, player, companion);
    }

    /**
     * Parses the given string token to {@link EventType}
     *
     * @param pToken
     * @return {@link EventType}
     * @since 25.10.2013
     */
    protected EventType parseEventType(String pToken)
    {
        long id = Long.parseLong(pToken.substring(pToken.indexOf('{') + 1, pToken.indexOf('}')));
        return EventType.valueOfId(id);
    }

    /**
     * Amount examples:
     *
     * <pre>
     * "(1447* energy {836045448940874})"
     * "(0 -miss {836045448945502})"
     * "(1969  (19 absorbed {836045448945511})"
     * "(1458)"
     * </pre>
     */
    protected Amount parseAmount(String pToken)
    {
        // '(' + ')' = 2 characters long
        if (pToken == null || pToken.length() <= 2)
        {
            return null;
        }

        int spaceIdx = pToken.indexOf(' ');
        if (spaceIdx > -1)
        {
            pToken = pToken.substring(1, spaceIdx);
        }
        else
        {
            pToken = pToken.substring(1, pToken.length());
        }

        Amount a = new Amount();
        if (pToken.charAt(pToken.length() - 1) == '*')
        {
            a.setCritical(true);
            pToken = pToken.substring(0, pToken.length() - 1);
        }
        a.setAmount(Integer.parseInt(pToken));
        return a;
    }

    /**
     * parses given string token to {@link Mitigation}<br>
     * Mitigation examples:
     *
     * <pre>
     * "(1447* energy {836045448940874})"
     * "(0 -miss {836045448945502})"
     * "(1969  (19 absorbed {836045448945511})"
     * "(1458)" -> no mitigation
     * </pre>
     *
     * @return {@link Mitigation}
     * @see Mitigation
     */
    protected Mitigation parseMitigation(String pToken)
    {
        int fIndex = pToken.lastIndexOf('{');
        int lIndex = pToken.lastIndexOf('}');
        if (fIndex > -1 && lIndex > -1 && lIndex > fIndex)
        {
            Mitigation m = new Mitigation();
            long id = Long.parseLong(pToken.substring(fIndex + 1, lIndex));
            m.setMitigationType(MitigationType.valueOfId(id));
            if (m.getMitigationType() == MitigationType.ABSORBED)
            {
                int bracketOpen = pToken.indexOf('(', 2);
                if (bracketOpen > -1)
                {
                    pToken = pToken.substring(bracketOpen + 1, pToken.indexOf(' ', bracketOpen));
                    int mitigated = Integer.parseInt(pToken);
                    m.setAmountMitigated(mitigated);
                }
            }
            return m;
        }
        return null;
    }

    /**
     * Example token:
     *
     * <pre>
     * Prototype Nano-Infused Skill Stim {3149903245082624}
     * </pre>
     *
     * @param pToken
     * @return {@link Ability} or {@code null}
     * @since 25.10.2013
     */
    protected Ability parseAbility(String pToken, int pFromIndex)
    {
        int bracketOpenIdx = pToken.indexOf('{', pFromIndex);
        long id = Long.parseLong(pToken.substring(bracketOpenIdx + 1, pToken.indexOf('}', pFromIndex)));
        if (id > 0)
        {
            String name = pToken.substring(pFromIndex, bracketOpenIdx - 1).trim();
            if (name.length() == 0)
            {
                name = UNDEFINED_ABILITY_NAME;
            }
            return new Ability(name, id);
        }
        return null;
    }

    @Override
    public LogEntry parseLogEntry(String pLine, FastDateParser pDateParser) throws ParseException
    {
        String tokens[] = SPLIT_PATTERN.split(pLine);
        LogEntry e = new LogEntry();

        for (int i = 0; i < tokens.length; i++)
        {
            String token = tokens[i];
            switch (i)
            {
                case 0:
                    e.setDate(pDateParser.parse(token.substring(1)));
                    break;

                case 1:
                    e.setCharacterSource(parseActor(token));
                    break;

                case 2:
                    e.setCharacterTarget(parseActor(token));
                    break;

                case 3:
                    if(token.length() > 2)
                    {
                        e.setAbility(parseAbility(token, 0));
                    }
                    break;

                case 4:
                    int midIdx = token.indexOf(':');
                    e.setSecondaryAbility(parseAbility(token, midIdx + 1));
                    e.setEventType(parseEventType(token));

                    int nextTokenStartIdx = token.indexOf('}', midIdx);

                    String subToken = token.substring(nextTokenStartIdx);

                    int amountTokenStartIdx = subToken.indexOf('(');
                    if (amountTokenStartIdx > -1)
                    {
                        int amountTokenEndIdx = subToken.indexOf(')');
                        if (amountTokenEndIdx > -1 && amountTokenEndIdx > amountTokenStartIdx)
                        {
                            String amountToken = subToken.substring(amountTokenStartIdx, amountTokenEndIdx);
                            e.setAmount(parseAmount(amountToken));
                        }
                    }

                    int threatStartIdx = subToken.lastIndexOf('<');
                    if (threatStartIdx > -1)
                    {
                        int threatEndIdx = subToken.lastIndexOf('>');
                        if (threatEndIdx > -1 && threatEndIdx > threatStartIdx)
                        {
                            e.setThreat(Integer.parseInt(subToken.substring(threatStartIdx + 1, threatEndIdx)));
                        }
                    }

                    break;

                default:
                    break;
            }
        }
        return e;
    }
}
