package me.eckig.swtor.io.parser;

import me.eckig.swtor.data.LogEntry;


/**
 *
 * @author eckig
 * @since 09.07.2013
 */
public interface ILineParser
{

    /**
     * Constant name for an ability with no/empty name
     */
    public static final String UNDEFINED_ABILITY_NAME = "?";

    /**
     * @param pLine
     *            the log line to parse
     * @param pDateParser
     *            date parser
     * @return {@link LogEntry}
     * @throws Exception
     * @since 09.07.2013
     */
    public LogEntry parseLogEntry(String pLine, FastDateParser pDateParser) throws Exception;
}
