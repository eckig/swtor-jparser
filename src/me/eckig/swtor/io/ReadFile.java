package me.eckig.swtor.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import me.eckig.swtor.Parser;
import me.eckig.swtor.data.LogEntry;
import me.eckig.swtor.io.parser.ILineParser;
import me.eckig.swtor.parser.comm.IParserUpdate;
import me.eckig.swtor.parser.comm.ISingleParserUpdate;


/**
 * Extension of {@link TailHandler} used to read a file just once from start to
 * end and publish the resulting {@link LogEntry} objects to a
 * {@link ISingleParserUpdate}.
 *
 * @author eckig
 * @since 04.11.2013
 */
public class ReadFile extends TailHandler implements Runnable
{

    private ISingleParserUpdate mReceiver;
    private File mFile;

    /**
     * Constructor
     *
     * @param pParser
     * @param pReceiver
     * @param pLineParser
     * @param pFile
     * @since 04.11.2013
     */
    public ReadFile(Parser pParser, ISingleParserUpdate pReceiver, ILineParser pLineParser, File pFile)
    {
        super(pParser, pLineParser);
        mReceiver = pReceiver;
        mFile = pFile;
    }

    @Override
    protected List<IParserUpdate> getSubscribers()
    {
        List<IParserUpdate> subs = new ArrayList<>(1);
        subs.add(mReceiver);
        return subs;
    }

    @Override
    public void run()
    {
        mReceiver.started();
        try (BufferedReader br = new BufferedReader(new FileReader(mFile));)
        {
            String line;
            while ((line = br.readLine()) != null)
            {
                handle(line);
            }
        }
        catch (Exception e)
        {
            getErrorHandler().handle(e);
        }
        finally
        {
            mReceiver.stopped();
        }
    }
}
