package me.eckig.swtor.io;

import java.util.List;

import me.eckig.swtor.Parser;
import me.eckig.swtor.data.IIDs;
import me.eckig.swtor.data.LogEntry;
import me.eckig.swtor.data.enums.EventType;
import me.eckig.swtor.io.parser.FastDateParser;
import me.eckig.swtor.io.parser.ILineParser;
import me.eckig.swtor.parser.comm.IErrorHandler;
import me.eckig.swtor.parser.comm.IParserUpdate;
import me.eckig.swtor.parser.comm.ITailListener;
import me.eckig.swtor.parser.exception.ParseException;
import me.eckig.swtor.parser.exception.PublishException;


/**
 * {@link ITailListener} that receives every single line read by {@link Tailer}
 * .<br>
 * Each line will then be parsed into a {@link LogEntry} with an instance of
 * {@link ILineParser}.
 *
 * @author eckig
 * @since 29.05.2013
 */
public class TailHandler implements ITailListener
{

    private boolean mInCombat = false;
    private FastDateParser mDateParser;
    private IErrorHandler mHandler;
    private Parser mParser;
    private ILineParser mLineParser;

    /**
     * Constructor
     *
     * @param pParser
     *            {@link Parser}
     * @param pLineParser
     *            {@link ILineParser} implementation
     * @since 29.05.2013
     */
    public TailHandler(Parser pParser, ILineParser pLineParser)
    {
        if (pParser == null)
        {
            throw new IllegalArgumentException("Parser can not be null!");
        }
        if (pLineParser == null)
        {
            throw new IllegalArgumentException("ILineParser implementation can not be null!");
        }
        mParser = pParser;
        mLineParser = pLineParser;
    }

    /**
     * set {@link IErrorHandler}
     *
     * @param pHandler
     * @since 10.10.2013
     */
    public void setErroHandler(IErrorHandler pHandler)
    {
        mHandler = pHandler;
    }

    /**
     * Set the day derived from the file name currently being monitored
     *
     * @param pDay
     * @throws IllegalArgumentException
     *             if date is not in the format {@code yyyy-MM-dd}
     * @since 29.05.2013
     */
    public void setDay(String pDay) throws IllegalArgumentException
    {
        mDateParser = new FastDateParser(pDay);
    }

    @Override
    public void handle(String line)
    {
        try
        {
            if (line == null || line.length() == 0)
            {
                return;
            }

            LogEntry dobj = mLineParser.parseLogEntry(line, mDateParser);
            publishLogEntry(dobj);
        }
        catch (Exception e)
        {
            if (getErrorHandler() != null)
            {
                getErrorHandler().handle(new ParseException(e, line));
            }
        }
    }

    /**
     * @return {@link List} of {@link IParserUpdate}
     * @since 04.11.2013
     */
    protected List<IParserUpdate> getSubscribers()
    {
        return mParser.getSubscribers();
    }

    /**
     * Publish a {@link LogEntry}
     *
     * @param pObj
     *            {@link LogEntry} to publish
     * @since 28.05.2013
     */
    protected void publishLogEntry(LogEntry pObj)
    {
        if (pObj.getEventType() == EventType.EVENT && pObj.getSecondaryAbility() != null
                && pObj.getSecondaryAbility().getId() == IIDs.ID_ENTER_COMBAT)
        {
            mInCombat = true;
        }

        for (IParserUpdate u : getSubscribers())
        {
            if (u.includeCompanions() == false && pObj.isCompanionLogEntry())
            {
                continue; // Ignore companions
            }

            if (u.includeOutOfCombat() == false && mInCombat == false)
            {
                continue; // ignore out of fight segments
            }

            try
            {
                u.newLogEntry(pObj);
            }
            catch (Throwable e)
            {
                if (getErrorHandler() != null)
                {
                    getErrorHandler().handle(new PublishException(e, u, pObj));
                }
                continue;
            }
        }

        if (pObj.getSecondaryAbility() != null && pObj.getSecondaryAbility().getId() == IIDs.ID_EXIT_COMBAT)
        {
            mInCombat = false;
        }
        // 'Safe Login Immunity' is another safe indicator that a fight has ended
        else if (pObj.getAbility() != null
                && (pObj.getAbility().getId() == IIDs.SAFE_LOGIN_IMMUNITY_ID || pObj.getSecondaryAbility() != null
                        && pObj.getSecondaryAbility().getId() == IIDs.SAFE_LOGIN_IMMUNITY_ID))
        {
            mInCombat = false;
        }
    }

    /**
     * @return {@link IErrorHandler}
     * @since 20.11.2013
     */
    protected IErrorHandler getErrorHandler()
    {
        return mHandler;
    }
}
