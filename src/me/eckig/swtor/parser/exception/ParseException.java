package me.eckig.swtor.parser.exception;


/**
 * Exception thrown when a line of a log file could not be parsed.
 * 
 * @author eckig
 * @since 24.10.2013
 */
public class ParseException extends Exception
{

    /**
     * @since 24.10.2013
     */
    private static final long serialVersionUID = -325736963133136657L;

    private String mLine;

    /**
     * Constructor
     *
     * @param pNestedException
     *            {@link Throwable}
     * @param pLine
     *            line which could not be parsed
     * @since 24.10.2013
     */
    public ParseException(Throwable pNestedException, String pLine)
    {
        super(pNestedException);
        mLine = pLine;
    }

    /**
     * @return the line which could not be parsed.
     * @since 24.10.2013
     */
    public String getLine()
    {
        return mLine;
    }

}
