package me.eckig.swtor.parser.exception;

import me.eckig.swtor.data.LogEntry;
import me.eckig.swtor.parser.comm.IParserUpdate;


/**
 * Exception thrown when a parsed log file entry could not be published to a
 * subscriber.
 *
 * @author eckig
 * @since 24.10.2013
 */
public class PublishException extends Exception
{

    /**
     * @since 24.10.2013
     */
    private static final long serialVersionUID = 5527395853800617699L;

    private IParserUpdate mSubscriber;
    private LogEntry mEntry;

    /**
     * Constructor
     *
     * @param pNestedException
     *            {@link Throwable}
     * @param pReceiver
     *            {@link IParserUpdate}
     * @param pEntry
     *            {@link LogEntry}
     * @since 24.10.2013
     */
    public PublishException(Throwable pNestedException, IParserUpdate pReceiver, LogEntry pEntry)
    {
        super(pNestedException);
        mSubscriber = pReceiver;
        mEntry = pEntry;
    }

    /**
     * @return {@link LogEntry} which could not be published.
     */
    public LogEntry getEntry()
    {
        return mEntry;
    }

    /**
     * @return {@link IParserUpdate} to which the {@link #getEntry()} could not
     *         be published.
     * @since 24.10.2013
     */
    public IParserUpdate getSubscriber()
    {
        return mSubscriber;
    }

}
