package me.eckig.swtor.parser.comm;

import me.eckig.swtor.io.Tailer;


/**
 * Listener for error events from a {@link Tailer}.
 *
 * @since 18.11.2013
 */
public interface ITailListener
{
    
    /**
     * Handles a line from a Tailer.
     * <p>
     * <b>Note:</b> this is called from the tailer thread.
     * </p>
     * 
     * @param pLine
     *            the line.
     */
    public void handle(String pLine);
}
