package me.eckig.swtor.parser.comm;

import java.io.File;

import me.eckig.swtor.io.Tailer;


/**
 * Listener for events from a {@link Tailer}.
 *
 * @since 18.11.2013
 */
public interface ITailErrorListener
{

    /**
     * Handle an Exception that caused the {@link Tailer} to stop.<br>
     * <b>Note:</b> this is called from the tailer thread.
     *
     * @param pExc
     *            {@link Exception}
     * @param pFile
     *            file that was tailed
     */
    public void handle(Exception pExc, File pFile);
}
