package me.eckig.swtor.parser.comm;

import java.io.File;

import me.eckig.swtor.parser.exception.ParseException;
import me.eckig.swtor.parser.exception.PublishException;


/**
 * Interface to handle Exceptions
 *
 * @author eckig
 * @since 29.05.2013
 */
public interface IErrorHandler
{

    /**
     * Handle a {@link ParseException} which is thrown if a line of a log file
     * could not be parsed.
     *
     * @param pException
     *            {@link ParseException}
     * @since 24.10.2013
     */
    public void handle(ParseException pException);

    /**
     * Handle a {@link PublishException} which is thrown if a parsed entry of a
     * log file could not be published to a subscriber.
     *
     * @param pException
     *            {@link PublishException}
     * @since 24.10.2013
     */
    public void handle(PublishException pException);

    /**
     * Handle a general {@link Exception}
     * 
     * @param pException
     *            {@link Exception}
     * @since 24.10.2013
     */
    public void handle(Exception pException);

    /**
     * Called whenever no suitable file has been found to parse
     *
     * @param pDirectory
     *            directory as {@link File}
     * @since 24.10.2013
     */
    public void handleNoFileFound(File pDirectory);
}
