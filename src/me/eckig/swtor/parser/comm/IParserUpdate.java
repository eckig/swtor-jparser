package me.eckig.swtor.parser.comm;

import me.eckig.swtor.data.Actor;
import me.eckig.swtor.data.LogEntry;


/**
 * <p>
 * Interface for a subscriber keeping track of {@link LogEntry} objects.
 * </p>
 * <p>
 * If you wish to keep track of combats, examine the objects for
 * {@link LogEntry#isFirstLogEntryOfCombat()} to test if a new combat has
 * started. Additionally the first entry of a combat can be used to determine
 * the current players {@link Actor} with {@link LogEntry#getCharacterSource()}.
 * </p>
 *
 * @author eckig
 */
public interface IParserUpdate
{

    /**
     * Notifies on a new {@link LogEntry}
     *
     * @param pNewData
     *            {@link LogEntry} which has been added
     * @since 29.05.2013
     */
    public void newLogEntry(LogEntry pNewData);

    /**
     * @return {@code true} to include companions in update notifications or
     *         {@code false} to exclude them.
     * @since 22.10.2013
     */
    public boolean includeCompanions();

    /**
     * @return {@code true} to include data out of combat or {@code false} to
     *         only receive updates on in combat segments.
     * @since 22.10.2013
     */
    public boolean includeOutOfCombat();
}
