package me.eckig.swtor.parser.comm;

import me.eckig.swtor.Parser;


/**
 * Extension of {@link IParserUpdate} for
 * {@link Parser#readFile(ISingleParserUpdate, java.io.File)}.
 * 
 * @author eckig
 * @since 04.11.2013
 */
public interface ISingleParserUpdate extends IParserUpdate
{

    /**
     * called before trying to read the file.
     *
     * @since 04.11.2013
     */
    public void started();

    /**
     * called when the file has been read.
     *
     * @since 04.11.2013
     */
    public void stopped();
}
