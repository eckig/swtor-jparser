package me.eckig.swtor.parser.comm;



/**
 * Status listener informing on the current parser state
 *
 * @author eckig
 * @since 19.11.2013
 */
public interface IStatusListener
{

    /**
     * Called when the parser has been started and is running.
     *
     * @since 19.11.2013
     */
    public void started();

    /**
     * Called when the parser has started tailing a new file.
     * 
     * @param pFileName
     *            filename of the new file.
     * @since 21.11.2013
     */
    public void parsingFile(String pFileName);

    /**
     * Called when the parser has stopped tailing a file because of an error.
     * 
     * @param pFileName
     *            filename of the file that was tailed.
     * @since 21.11.2013
     */
    public void lostFile(String pFileName);

    /**
     * Called when the monitor has been stopped.
     *
     * @since 19.11.2013
     */
    public void stopped();
}
