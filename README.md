# SW:ToR CombatLog Parser

Mature, fast & lightweight Java API for reading and parsing [Star Wars: The Old Republic](http://www.swtor.com/) combat log files.

The Parser will parse every suitable line of a log file into a LogEntry Object.
Detection how and when a Combat has started and ended is handled by the Parser.

Furthermore the parser will automatically watch the configured directory for new log files and will scan them continuously for new lines.

## Basic usage

    // Create the parser:
    // The file argument is expected to be the 'CombatLogs' folder.
    // In most cases Parser.getLogDirBySystemProperty() should suffice, 
    // but you may choose another directory or let the user configure this
    Parser p = new Parser(File);

    // each (you may register as many subscribers as you like) IParserUpdate
    // instance will receive updates on new segments.
    p.addListener(IParserUpdate);
    
    // optional: register an error handler
    p.setErrorHandler(IErrorHandler);
    
    // start parsing
    p.start();
    
    // and don't forget to stop:
    p.stop();

## Reference Implementation

 - [MSE-6](https://bitbucket.org/eckig/mse-6)